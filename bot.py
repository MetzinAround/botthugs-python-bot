import os
import tweepy
from dotenv import load_dotenv

load_dotenv()

# Authenticate to Twitter
auth = tweepy.OAuthHandler(os.getenv('consumer_key_1'), os.getenv('consumer_secret_1'))
auth.set_access_token(os.getenv('access_token_1'), os.getenv('access_token_secret_1'))


api = tweepy.API(auth)

#This is to send a single update. We're gonna need to make it so it can tweet on a schedule, which I currently don't know how to do. yet. 
#api.update_status("testing again y'all")