import os
from dotenv import load_dotenv

load_dotenv()
consumer_key = os.getenv("consumer_key_m")
consumer_secret = os.getenv("consumer_secret_m")
access_token = os.getenv("access_token_m")
access_token_secret = os.getenv("access_token_secret_m")

